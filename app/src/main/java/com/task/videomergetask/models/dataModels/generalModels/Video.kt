package com.task.videomergetask.models.dataModels.generalModels

import android.net.Uri

data class Video(
    val id: Long,
    val name: String,
    val duration: Int,
    val uri: Uri,
    val filePath: String
)