package com.task.videomergetask.views.adapters


import android.view.View
import com.bumptech.glide.Glide
import com.task.videomergetask.R
import com.task.videomergetask.models.dataModels.generalModels.Video
import kotlinx.android.synthetic.main.rv_video_item.view.*


class VideoListAdapter(
    private val videosList: ArrayList<Video>,
    private val itemClicker: BaseAdapter.OnItemClicker
) : BaseAdapter(videosList,itemClicker,R.layout.rv_video_item) {

    override fun View.bind(item: Any, position: Int) {
        val data=item as Video
        this.tvVideoName.text=data.name
        this.tvVideoDuration.text="Duration : "+data.duration.toString()
        Glide.with(context).load(data.uri).into(ivVideoImage)
    }
}