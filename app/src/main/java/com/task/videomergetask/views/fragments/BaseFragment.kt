package com.task.videomergetask.views.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.Navigation

import com.task.videomergetask.R
import com.task.videomergetask.utils.generalUtils.DialogUtils
import com.task.videomergetask.viewModels.BaseViewModel
import com.task.videomergetask.views.activities.MainActivity
import kotlinx.android.synthetic.main.activity_main.*


abstract class BaseFragment(resId: Int) : Fragment(resId) {

    lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = DialogUtils.getProgressDialog(requireContext())
    }


    abstract fun initViews()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }


    fun showProgressDialog(show: Boolean) {
        if (dialog != null && show) {
            if (!dialog.isShowing)
                dialog.apply {
                    show()
                }
        } else if (dialog != null && !show) {
            if (dialog.isShowing)
                dialog.dismiss()
        }
    }





    fun navigateToFragment(navDirections: NavDirections) {
        val navController =
            Navigation.findNavController(activity as MainActivity, R.id.nav_host_fragment)
        navController.navigate(navDirections)
    }


    protected fun setupGeneralViewModel(generalViewModel: BaseViewModel) {

        with(generalViewModel)
        {
            snackbarMessage.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let {
                    showToast(it)
                }
            })

            progressBar.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let { showProgressDialog(it) }
            })
        }
    }


    protected fun onBackPress() {
        requireActivity().onBackPressed()
    }

    protected fun setToolbarTitle(
        title: String,
        isBackHide: Boolean, isHideExitButton:Boolean
    ) {
        (requireActivity() as MainActivity).setToolbarTitle(title, isBackHide, isHideExitButton)
    }
    protected fun setToolbarIconClick(){
        (activity as MainActivity).ivBack?.setOnClickListener {
            onBackPress()
        }
    }

    fun showToast(message:String){
        Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT).show()
    }




}
