package com.task.videomergetask.views.activities

import androidx.lifecycle.lifecycleScope
import com.task.videomergetask.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : BaseActivity() {
    companion object {
        val SPLASH_DELAY: Long = 2000
    }

    override fun initViews() {
        lifecycleScope.launch {
            delay(SPLASH_DELAY)
            gotoMainActivity()
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_splash

}
