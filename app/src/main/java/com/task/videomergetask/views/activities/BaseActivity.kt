package com.task.videomergetask.views.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.task.videomergetask.utils.generalUtils.DialogUtils


abstract class BaseActivity : AppCompatActivity() {

    var dialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.MODE_NIGHT_YES
        setContentView(getLayoutId())
        dialog = DialogUtils.getProgressDialog(this)
        initViews()
    }

    abstract fun getLayoutId(): Int
    abstract fun initViews()

    fun gotoMainActivity() {
       startActivity(Intent(this,MainActivity::class.java))
       finish()
    }





}
