package com.task.videomergetask.views.fragments

import android.Manifest
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.task.videomergetask.R
import com.task.videomergetask.models.dataModels.generalModels.Video

import com.task.videomergetask.viewModels.VideosViewModel
import com.task.videomergetask.views.activities.MainActivity
import com.task.videomergetask.views.adapters.BaseAdapter
import com.task.videomergetask.views.adapters.VideoListAdapter
import com.task.videomergetask.views.dialog.ConfirmationDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment(R.layout.fragment_home), BaseAdapter.OnItemClicker {


    private val allVideos: ArrayList<Video> = ArrayList()
    private val videosAdapter by lazy { VideoListAdapter(allVideos, this) }
    private val viewModel: VideosViewModel by viewModel()


    override fun initViews() {
        setToolbarTitle(getString(R.string.users_list), false, true)
        (activity as MainActivity).ivExit.setOnClickListener {
            confirmToExit()
        }
        rvVideos.adapter = videosAdapter
        btnMerge.setOnClickListener {
            if (allVideos.size < 3) {
                showToast("Please create 3 videos to merge")
            } else {
                showProgressDialog(true)
                viewModel.mergeVideos(requireContext(),ArrayList(allVideos.subList(0,3)))
            }
        }
        setupViewModel()

    }

    private fun setupViewModel() {
        with(viewModel) {
            setupGeneralViewModel(this)
            videosLiveData.observe(viewLifecycleOwner) {
                allVideos.clear()
                allVideos.addAll(it)
                videosAdapter.notifyDataSetChanged()
            }
            completedVideoLiveData.observe(viewLifecycleOwner) {
                showProgressDialog(false)
                if (it != null) {
                    startLoadingVideos()
                    showToast("Video merged successfully")
                }
            }
            startLoadingVideos()
        }
    }

    private fun startLoadingVideos() {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        Permissions.check(requireContext(), permissions, null, null, object : PermissionHandler() {
            override fun onGranted() {
                viewModel.loadVideos(requireContext())
            }
        })
    }


    private fun confirmToExit() {
        ConfirmationDialog(false,
            getString(R.string.sure_to_exit_application),
            getString(R.string.alert),
            object : ConfirmationDialog.ConfirmationListener {
                override fun isConfirmed(isConfirmed: Boolean) {
                    if (isConfirmed) {
                        activity?.finish()
                    }
                }
            }).show(childFragmentManager, "")
    }

    override fun onItemClick(position: Int, data: Any) {

    }
}