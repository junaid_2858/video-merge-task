package com.task.videomergetask.views.activities

import android.view.View
import com.task.videomergetask.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun initViews() {

    }

    fun setToolbarTitle(
        title: String,
        backHide: Boolean = false,
        hideExitButton: Boolean
    ) {
        tvToolbarName.text = title
        if(backHide)
        {
            ivBack.visibility=View.VISIBLE
            ivExit.visibility=View.GONE
        }
        else if(hideExitButton)
        {
            ivExit.visibility=View.VISIBLE
            ivBack.visibility=View.GONE
        }
        else{
            ivBack.visibility=View.GONE
            ivExit.visibility=View.GONE
        }
    }
}
