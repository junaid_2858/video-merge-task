package com.task.videomergetask.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.task.videomergetask.models.dataModels.generalModels.Video
import com.task.videomergetask.utils.VideoProcessor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class VideosViewModel() : BaseViewModel() {
    var videosLiveData: MutableLiveData<List<Video>> = MutableLiveData()
    var completedVideoLiveData: MutableLiveData<Video?> = MutableLiveData()

    fun loadVideos(context: Context) {
        viewModelScope.launch {
            val results=async {
                VideoProcessor().run(context)
            }
            videosLiveData.value = results.await()
        }
    }

    fun mergeVideos(context: Context,videosList:ArrayList<Video>) {
        viewModelScope.launch(Dispatchers.IO){
            val result=async { VideoProcessor().mergeSelectedVideos(context, videosList) }
            val videoResult=result.await()
            withContext(Dispatchers.Main)
            {
                completedVideoLiveData.value=videoResult
            }

        }
    }
}