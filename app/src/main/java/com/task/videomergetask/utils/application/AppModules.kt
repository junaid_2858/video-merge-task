package com.task.videomergetask.utils.application

import com.task.videomergetask.viewModels.VideosViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module



val viewModelModules= module {
    viewModel {
        VideosViewModel()
    }
}